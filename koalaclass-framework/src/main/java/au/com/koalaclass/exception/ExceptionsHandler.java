package au.com.koalaclass.exception;

import au.com.koalaclass.entity.AjaxResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author Superlu
 * @version 1.0.0
 * @ClassName ExceptionsHandler
 * @Description 统一封装异常返回结果
 * @createTime 2021/11/4 9:28
 */

@Slf4j
@RestControllerAdvice
public class ExceptionsHandler {

    @ExceptionHandler(value = Exception.class)
    public AjaxResult exception(Exception ex) {
        log.error("服务器异常：{}", ex.getMessage(), ex);
        return AjaxResult.error("服务器错误");
    }

    @ExceptionHandler(value = ServiceException.class)
    public AjaxResult ServiceException(ServiceException serviceException) {
        log.error("业务异常：{}", serviceException.getMessage(),serviceException);
        return AjaxResult.error(serviceException.getData(), serviceException.getMessage(), serviceException.getCode());
    }
}
