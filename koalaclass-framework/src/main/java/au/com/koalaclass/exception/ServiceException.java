package au.com.koalaclass.exception;

import lombok.Getter;

/**
 * @author Superlu
 * @version 1.0.0
 * @ClassName ServiceException
 * @Description 自定义异常的基类
 * @createTime 2021/11/5 14:22
 */
@Getter
public class ServiceException extends RuntimeException{

    //代码
    private int code;

    private Object data;

    public ServiceException(String message) {
        super(message);
        this.code = 500;
    }

    public ServiceException(String message, int code) {
        super(message);
        this.code = code;
    }
    public ServiceException( String message,Object data) {
        super(message);
        this.code = 500;
        this.data = data;
    }
    public ServiceException( String message, int code,Object data) {
        super(message);
        this.code = code;
        this.data = data;

    }



}
