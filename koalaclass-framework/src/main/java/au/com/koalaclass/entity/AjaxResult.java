package au.com.koalaclass.entity;

import lombok.*;

/**
 * @author Superlu
 * @version 1.0.0
 * @ClassName BaseResult
 * @Description TODO
 * @createTime 2021/11/2 15:49
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AjaxResult {
    private Integer code;
    private String message;
    private Object data;

    public static AjaxResult success() {
        return success(null);
    }

    public static AjaxResult success(Object data) {
        return AjaxResult.builder().code(200).message("Success").data(data).build();
    }

    public static AjaxResult error(Object data, String message) {
        return error(data,message,500);
    }

    public static AjaxResult error(Object data, String message, int code) {
        return AjaxResult.builder().code(code).message(message).data(data).build();
    }

    public static AjaxResult error(String message, int code) {
        return error(null,message,code);
    }
    public static AjaxResult error(String message) {
        return error(message,500);
    }
}
