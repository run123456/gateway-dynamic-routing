/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50728
 Source Host           : localhost:3306
 Source Schema         : gateway

 Target Server Type    : MySQL
 Target Server Version : 50728
 File Encoding         : 65001

 Date: 16/11/2021 13:16:00
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gateway_route
-- ----------------------------
DROP TABLE IF EXISTS `gateway_route`;
CREATE TABLE `gateway_route`  (
  `id` bigint(28) NOT NULL AUTO_INCREMENT,
  `service_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `uri` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '转发地址',
  `predicates` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '访问路径',
  `filters` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '过滤',
  `route_order` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '顺序',
  `create_time` bigint(20) NULL DEFAULT NULL,
  `update_time` bigint(20) NULL DEFAULT NULL,
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gateway_route
-- ----------------------------
INSERT INTO `gateway_route` VALUES (1, 'serviceNode1', 'https://baidu.com/', '[{\r\n	\"type\":\"Path\",\r\n  \"predicates\":\"/api-baidu/**\"\r\n}]', '[\r\n  {\r\n    \"type\": \"StripPrefix\",\r\n    \"rule\": \"1\"\r\n  }\r\n]', '0', 1637031315328, 1637031315328, NULL, '0');
INSERT INTO `gateway_route` VALUES (2, 'serviceNode2', 'https://www.taobao.com/', '[{\r\n  \"type\": \"Path\",\r\n  \"predicates\": \"/api-taobao/**\"\r\n}]', '[\r\n  {\r\n    \"type\": \"StripPrefix\",\r\n    \"rule\": \"1\"\r\n  }\r\n]', '0', 1637031315328, 1637031315328, NULL, '0');
INSERT INTO `gateway_route` VALUES (8, 'serviceNode3', 'https://youxiu326.xin/', '[{\r\n  \"type\": \"Path\",\r\n  \"predicates\": \"/youxiu326/**\"\r\n}]', '[\r\n  {\r\n    \"type\": \"StripPrefix\",\r\n    \"rule\": \"1\"\r\n  }\r\n]', '1', 1637031315328, 1637031315328, NULL, '0');
INSERT INTO `gateway_route` VALUES (10, 'serviceNode4', 'https://suggest.taobao.com/', '[{\r\n  \"type\": \"Path\",\r\n  \"predicates\": \"/search/**\"\r\n}]', '[\r\n  {\r\n    \"type\": \"StripPrefix\",\r\n    \"rule\": \"1\"\r\n  }\r\n]', '1', 1637031315328, 1637031315328, NULL, '0');
INSERT INTO `gateway_route` VALUES (11, 'serviceNode5', 'https://www.jd.com/', '[{\r\n  \"type\": \"Path\",\r\n  \"predicates\": \"/api-jd/**\"\r\n}]', '[\r\n  {\r\n    \"type\": \"StripPrefix\",\r\n    \"rule\": \"1\"\r\n  }\r\n]', '0', 1637031315328, 1637031315328, NULL, '0');
INSERT INTO `gateway_route` VALUES (13, 'service-csdn', 'https://blog.csdn.net/', '[{\r\n  \"type\": \"Path\",\r\n  \"predicates\": \"/api-csdn/**\"\r\n}]', '[\r\n  {\r\n    \"type\": \"StripPrefix\",\r\n    \"rule\": \"1\"\r\n  }\r\n]', '0', 1637031315328, 1637031315328, NULL, '0');
INSERT INTO `gateway_route` VALUES (14, 'nacos-getway-provider', 'nacos-provider', '[{\r\n  \"type\": \"Path\",\r\n  \"predicates\": \"/provider/**\"\r\n}]', '[\r\n  {\r\n    \"type\": \"StripPrefix\",\r\n    \"rule\": \"1\"\r\n  }\r\n]', '0', 1637031315328, 1637031315328, NULL, '0');
INSERT INTO `gateway_route` VALUES (16, 'nacos-getway-weibo', 'https://weibo.com', '[{\r\n  \"type\": \"Path\",\r\n  \"predicates\": \"/weibo/**\"\r\n}]', '[\r\n  {\r\n    \"type\": \"StripPrefix\",\r\n    \"rule\": \"1\"\r\n  }\r\n]', '0', 1637031315328, 1637031315328, NULL, '0');
INSERT INTO `gateway_route` VALUES (21, 'consumer-add', 'service-consumer', '[{\"type\":\"Path\",\"predicates\":\"/consumer\"},{\"type\":\"Query\",\"predicates\":\"cmd,add\"}]', '[{\"type\":\"SetPath\",\"rule\":\"/add\"}]', '0', 0, 0, NULL, '0');
INSERT INTO `gateway_route` VALUES (22, 'consumer-del', 'service-consumer', '[{\"type\":\"Path\",\"predicates\":\"/consumer\"},{\"type\":\"Query\",\"predicates\":\"cmd,del\"}]', '[{\"type\":\"SetPath\",\"rule\":\"/del\"}]', '0', 0, 0, NULL, '0');

SET FOREIGN_KEY_CHECKS = 1;
