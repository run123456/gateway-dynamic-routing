package au.com.koalaclass;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**
 * @author Superlu
 * @version 1.0.0
 * @ClassName GateApplication.java
 * @Description TODO
 * @createTime 2021年10月29日 15:05:00
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableJpaAuditing
public class GatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class,args);
    }
}
