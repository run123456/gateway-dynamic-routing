package au.com.koalaclass.dao;


import au.com.koalaclass.entity.GatewayRouteEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GatewayRouteDao extends JpaRepository<GatewayRouteEntity, Long> {
}
