package au.com.koalaclass.entity;

import lombok.Data;

/**
 * @author Superlu
 * @version 1.0.0
 * @ClassName PredicatesEntity
 * @Description TODO
 * @createTime 2021/11/15 15:18
 */
@Data
public class PredicatesEntity {

    /**
     * 断言方式
     */
    private String type;

    /**
     * 断言规则
     */
    private String predicates;
}
