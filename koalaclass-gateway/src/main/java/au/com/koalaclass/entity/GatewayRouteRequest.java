package au.com.koalaclass.entity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;

import java.util.List;

/**
 * @author Superlu
 * @version 1.0.0
 * @ClassName GatewayRouteRequest
 * @Description TODO
 * @createTime 2021/11/15 16:13
 */
@Data
public class GatewayRouteRequest {

    private Long id;

    private String serviceId;

    private String uri;

    private List<PredicatesEntity> predicates;

    private List<FiltersEntity> filters;

    private String order;

    private String remarks;

    private String delFlag;

    public static GatewayRouteEntity toGatewayRouteEntity(GatewayRouteRequest request) throws JsonProcessingException {
        GatewayRouteEntity route = new GatewayRouteEntity();
        route.setId(request.getId());
        route.setServiceId(request.getServiceId());
        route.setUri(request.getUri());
        route.setPredicates(new ObjectMapper().writeValueAsString(request.getPredicates()));
        route.setFilters(new ObjectMapper().writeValueAsString(request.getFilters()));
        route.setOrder(request.getOrder());
        route.setRemarks(request.getRemarks());
        route.setDelFlag(request.getDelFlag());
        return route;
    }
}