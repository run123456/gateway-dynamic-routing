package au.com.koalaclass.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;

/**
 * 实体
 */
@Table(name = "gateway_route")
@Entity
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class GatewayRouteEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition = "int(11)")
    private Long id;

    @Column(name = "service_id")
    private String serviceId;

    @Column(name = "uri")
    private String uri;

    @Column(name = "predicates")
    private String predicates;

    @Column(name = "filters")
    private String filters;

    @Column(name = "route_order")
    private String order;

    @CreatedDate
    @Column(name = "create_time")
    private long createTime;

    @LastModifiedDate
    @Column(name = "update_time")
    private long updateTime;

    @Column(name = "remarks")
    private String remarks;

    @Column(name = "del_flag")
    private String delFlag;
}