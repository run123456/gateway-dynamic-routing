package au.com.koalaclass.entity;

import lombok.Data;

/**
 * @author Superlu
 * @version 1.0.0
 * @ClassName FiltersEntity
 * @Description TODO
 * @createTime 2021/11/16 10:33
 */
@Data
public class FiltersEntity {

    /**
     * 过滤器方式
     */
    private String type;

    /**
     * 过滤器规则
     */
    private String rule;
}
