package au.com.koalaclass;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author Superlu
 * @version 1.0.0
 * @ClassName DemoNacosApplication.java
 * @Description TODO
 * @createTime 2021年10月29日 11:08:00
 */
@SpringBootApplication
@EnableDiscoveryClient
public class NacosProviderApplication {
    public static void main(String[] args) {
        SpringApplication.run(NacosProviderApplication.class,args);
    }

}
