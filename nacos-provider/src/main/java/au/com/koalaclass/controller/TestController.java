package au.com.koalaclass.controller;

import org.springframework.web.bind.annotation.*;

/**
 * @author Superlu
 * @version 1.0.0
 * @ClassName TestController.java
 * @Description TODO
 * @createTime 2021年10月29日 15:38:00
 */
@RestController
@RequestMapping("/provider")
public class TestController {
    @RequestMapping(value = "/echo/p/{string}", method = RequestMethod.GET)
    public String echo(@PathVariable String string) {
        return "Hello Nacos Discovery " + string;
    }
    @GetMapping("/helloProvider")
    public String helloProvider(){
        return "你好,我是服务提供者";
    }
}
