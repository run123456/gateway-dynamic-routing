package au.com.koalaclass.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Superlu
 * @version 1.0.0
 * @ClassName AminController
 * @Description TODO
 * @createTime 2021/11/15 14:33
 */
@RestController
public class AminController {
    @RequestMapping("/")
    public String admin(){
        return "我是消费者";
    }

    @RequestMapping("/add")
    public String add(){
        return "Add Success";
    }

    @RequestMapping("/del")
    public String del(){
        return "Delete Success";
    }
}
