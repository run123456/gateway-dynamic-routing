package au.com.koalaclass.controller;

import au.com.koalaclass.service.ProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @author Superlu
 * @version 1.0.0
 * @ClassName TestController.java
 * @Description TODO
 * @createTime 2021年10月29日 15:36:00
 */
@RestController
@RequestMapping("/consumer")
public class TestController {

    @Resource
    private  RestTemplate restTemplate;

    @Resource
    private ProviderService providerService;

    @Autowired
    public TestController(RestTemplate restTemplate) {this.restTemplate = restTemplate;}

    @RequestMapping(value = "/echo/{str}", method = RequestMethod.GET)
    public String echo(@PathVariable String str) {
        return restTemplate.getForObject("http://nacos-provider/echo/p/" + str, String.class);
    }

    @RequestMapping(value = "/testFeign")
    public String testFeign(){
        return providerService.helloProvider();
    }
}