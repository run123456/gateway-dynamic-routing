package au.com.koalaclass.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author Superlu
 * @version 1.0.0
 * @ClassName RestTemplateConfig.java
 * @Description TODO
 * @createTime 2021年10月29日 15:36:00
 */
@Configuration
public class RestTemplateConfig {
    @LoadBalanced
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
