package au.com.koalaclass.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Superlu
 * @version 1.0.0
 * @ClassName ProviderService.java
 * @Description TODO
 * @createTime 2021年10月29日 16:02:00
 */
@Component
@FeignClient(value = "nacos-provider")
@RequestMapping("/provider")
public interface ProviderService {

    @GetMapping("/helloProvider")
    public String helloProvider();
}
